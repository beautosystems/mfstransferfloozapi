package com.mfs.client.transferflooz.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TransferFloozController {

	@GetMapping("/hi")
	public  String getMessage() {
		return "Welcome";		
	}
	
}
