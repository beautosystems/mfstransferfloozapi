package com.mfs.client.transferflooz.dao;

import com.mfs.client.transferflooz.exception.DaoException;

public interface BaseDao {

	public boolean save(Object obj) throws DaoException;

	public boolean update(Object obj) throws DaoException;

	public boolean saveOrUpdate(Object obj) throws DaoException;

}
