package com.mfs.client.transferflooz.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="transfer_flooz_transaction_error")
public class TransferFloozTransactionError {

	@Id
	@GeneratedValue
	@Column(name="transaction_error_id")
	private int transactionErrorId;
	
	@Column(name="status")
	private String status;
	
	@Column(name="message")
	private String message;
	
	@Column(name="reference_id")
	private String referenceId;
	
	@Column(name="transaction_id")
	private String transactionId;
	
	@Column(name="amount")
	private double amount;
	
	@Column(name="destination")
	private String destination;
	
	@Column(name="wallet_id")
	private String walletId;
	
	@Column(name="sender_country_code")
	private String senderCountryCode;
	
	@Column(name="sender_operator")
	private String senderOperator;
	
	
	@Column(name="extended_data")
	private String extendedData;
	
	@Column(name="date_logged")
	private Date dateLogged;

	public int getTransactionErrorId() {
		return transactionErrorId;
	}

	public void setTransactionErrorId(int transactionErrorId) {
		this.transactionErrorId = transactionErrorId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getSenderCountryCode() {
		return senderCountryCode;
	}

	public void setSenderCountryCode(String senderCountryCode) {
		this.senderCountryCode = senderCountryCode;
	}

	public String getSenderOperator() {
		return senderOperator;
	}

	public void setSenderOperator(String senderOperator) {
		this.senderOperator = senderOperator;
	}

	public String getExtendedData() {
		return extendedData;
	}

	public void setExtendedData(String extendedData) {
		this.extendedData = extendedData;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "TransferFloozTransactionError [transactionErrorId=" + transactionErrorId + ", status=" + status
				+ ", message=" + message + ", referenceId=" + referenceId + ", transactionId=" + transactionId
				+ ", amount=" + amount + ", destination=" + destination + ", walletId=" + walletId
				+ ", senderCountryCode=" + senderCountryCode + ", senderOperator=" + senderOperator + ", extendedData="
				+ extendedData + ", dateLogged=" + dateLogged + "]";
	}

		
}
