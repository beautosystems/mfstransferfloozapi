package com.mfs.client.transferflooz.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="transfer_flooz_transfer_status")
public class TransferFloozTransferStatus {
	
	@Id
	@GeneratedValue
	@Column(name="transfer_status_id")
	private int transferStatusId;
	
	@Column(name="status",length=20)
	private String status;
	
	@Column(name="message",length=150)
	private String message;
	
	@Column(name="reference_id",length=20)
	private String referenceId;
	
	@Column(name="transaction_id",length=20)
	private String transactionId;
	
	@Column(name="timestamp",length=20)
	private String timeStamp;
	
	@Column(name="amount",length=20)
	private String amount;
	
	@Column(name="destination",length=20)
	private String destination;
	
	@Column(name="date_logged")
	private Date dateLogged;

	public int getTransferStatusId() {
		return transferStatusId;
	}

	public void setTransferStatusId(int transferStatusId) {
		this.transferStatusId = transferStatusId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "TransferFloozTransferStatus [transferStatusId=" + transferStatusId + ", status=" + status + ", message="
				+ message + ", referenceId=" + referenceId + ", transactionId=" + transactionId + ", timeStamp="
				+ timeStamp + ", amount=" + amount + ", destination=" + destination + ", dateLogged=" + dateLogged
				+ "]";
	}	

}
