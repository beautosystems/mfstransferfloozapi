package com.mfs.client.transferflooz.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="transfer_flooz_audit_trail")
public class TransferFloozAuditTrail {
	
	@Id
	@GeneratedValue
	@Column(name="transaction_audit_id")
	private int transactionAuditId;
	
	@Column(name="status")
	private String status;
	
	@Column(name="message")
	private String message;
	
	@Column(name="reference_id")
	private String referenceId;
	
	@Column(name="transaction_id")
	private String transactionId;
	
	@Column(name="amount")
	private double amount;
	
	@Column(name="destination")
	private String destination;
	
	@Column(name="date_logged")
	private Date dateLogged;
	
	
	@Column(name="sender_country_code")
	private String senderCountryCode;
	
	@Column(name="sender_operator")
	private String senderOperator;

	@Column(name="service_name")
	private String serviceName;
	
	@Column(name="sender_balance_before")
	private String senderBalanceBefore;
	
	@Column(name="sender_balance_after")
	private String senderBalanceAfter;

	@Column(name="sender_key_cost")
	private String senderKeyCost;
	
	@Column(name="sender_bonus")
	private String senderBonus;

	@Column(name="wallet_id")
	private String walletId;

	public int getTransactionAuditId() {
		return transactionAuditId;
	}

	public void setTransactionAuditId(int transactionAuditId) {
		this.transactionAuditId = transactionAuditId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	public String getSenderCountryCode() {
		return senderCountryCode;
	}

	public void setSenderCountryCode(String senderCountryCode) {
		this.senderCountryCode = senderCountryCode;
	}

	public String getSenderOperator() {
		return senderOperator;
	}

	public void setSenderOperator(String senderOperator) {
		this.senderOperator = senderOperator;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getSenderBalanceBefore() {
		return senderBalanceBefore;
	}

	public void setSenderBalanceBefore(String senderBalanceBefore) {
		this.senderBalanceBefore = senderBalanceBefore;
	}

	public String getSenderBalanceAfter() {
		return senderBalanceAfter;
	}

	public void setSenderBalanceAfter(String senderBalanceAfter) {
		this.senderBalanceAfter = senderBalanceAfter;
	}

	public String getSenderKeyCost() {
		return senderKeyCost;
	}

	public void setSenderKeyCost(String senderKeyCost) {
		this.senderKeyCost = senderKeyCost;
	}

	public String getSenderBonus() {
		return senderBonus;
	}

	public void setSenderBonus(String senderBonus) {
		this.senderBonus = senderBonus;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	@Override
	public String toString() {
		return "TransferFloozAuditTrail [transactionAuditId=" + transactionAuditId + ", status=" + status + ", message="
				+ message + ", referenceId=" + referenceId + ", transactionId=" + transactionId + ", amount=" + amount
				+ ", destination=" + destination + ", dateLogged=" + dateLogged + ", senderCountryCode="
				+ senderCountryCode + ", senderOperator=" + senderOperator + ", serviceName=" + serviceName
				+ ", senderBalanceBefore=" + senderBalanceBefore + ", senderBalanceAfter=" + senderBalanceAfter
				+ ", senderKeyCost=" + senderKeyCost + ", senderBonus=" + senderBonus + ", walletId=" + walletId + "]";
	}
	
	

}
