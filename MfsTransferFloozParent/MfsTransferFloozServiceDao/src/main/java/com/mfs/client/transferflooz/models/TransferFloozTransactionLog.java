package com.mfs.client.transferflooz.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="transfer_flooz_transaction_log")
public class TransferFloozTransactionLog {
	
	@Id
	@GeneratedValue
	@Column(name="transaction_log_id")
	private int transactionLogId;
	
	@Column(name="status",length=20)
	private String status;
	
	@Column(name="message",length=150)
	private String message;
	
	@Column(name="reference_id",length=20)
	private String referenceId;
	
	@Column(name="transaction_id",length=20)
	private String transactionId;
	
	@Column(name="amount",length=20)
	private String amount;
	
	@Column(name="destination",length=20)
	private String destination;
	
	@Column(name="date_logged")
	private Date dateLogged;
	
	@Column(name="wallet_id",length=20)
	private String walletId;
	
	@Column(name="sender_Country_Code",length=20)
	private String senderCountryCode;
	
	@Column(name="sender_operator",length=20)
	private String senderOperator;
	
	@Column(name="extended_data",length=20)
	private String extendedData;
	
	@Column(name="sender_balance_before",length=20)
	private String senderBalanceBefore;
	
	@Column(name="sender_balance_after",length=20)
	private String senderBalanceAfter;
	
	@Column(name="sender_key_cost",length=20)
	private String senderKeyCost;

	@Column(name="sender_bonus",length=20)
	private String senderBonus;
	
	@Column(name="additional_info",length=20)
	private String additionalInfo;

	public int getTransactionLogId() {
		return transactionLogId;
	}

	public void setTransactionLogId(int transactionLogId) {
		this.transactionLogId = transactionLogId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	public String getWalletId() {
		return walletId;
	}

	public void setWalletId(String walletId) {
		this.walletId = walletId;
	}

	public String getSenderCountryCode() {
		return senderCountryCode;
	}

	public void setSenderCountryCode(String senderCountryCode) {
		this.senderCountryCode = senderCountryCode;
	}

	public String getSenderOperator() {
		return senderOperator;
	}

	public void setSenderOperator(String senderOperator) {
		this.senderOperator = senderOperator;
	}

	public String getExtendedData() {
		return extendedData;
	}

	public void setExtendedData(String extendedData) {
		this.extendedData = extendedData;
	}

	public String getSenderBalanceBefore() {
		return senderBalanceBefore;
	}

	public void setSenderBalanceBefore(String senderBalanceBefore) {
		this.senderBalanceBefore = senderBalanceBefore;
	}

	public String getSenderBalanceAfter() {
		return senderBalanceAfter;
	}

	public void setSenderBalanceAfter(String senderBalanceAfter) {
		this.senderBalanceAfter = senderBalanceAfter;
	}

	public String getSenderKeyCost() {
		return senderKeyCost;
	}

	public void setSenderKeyCost(String senderKeyCost) {
		this.senderKeyCost = senderKeyCost;
	}

	public String getSenderBonus() {
		return senderBonus;
	}

	public void setSenderBonus(String senderBonus) {
		this.senderBonus = senderBonus;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	@Override
	public String toString() {
		return "TransferFloozTransactionLog [transactionLogId=" + transactionLogId + ", status=" + status + ", message="
				+ message + ", referenceId=" + referenceId + ", transactionId=" + transactionId + ", amount=" + amount
				+ ", destination=" + destination + ", dateLogged=" + dateLogged + ", walletId=" + walletId
				+ ", senderCountryCode=" + senderCountryCode + ", senderOperator=" + senderOperator + ", extendedData="
				+ extendedData + ", senderBalanceBefore=" + senderBalanceBefore + ", senderBalanceAfter="
				+ senderBalanceAfter + ", senderKeyCost=" + senderKeyCost + ", senderBonus=" + senderBonus
				+ ", additionalInfo=" + additionalInfo + "]";
	}
	
	
	
}
