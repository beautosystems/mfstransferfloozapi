package com.mfs.client.transferflooz.dao;

import java.util.Map;
import com.mfs.client.transferflooz.exception.DaoException;

public interface SystemConfigDao {

	public Map<String, String> getConfigDetailsMap() throws DaoException;

}
