package com.mfs.client.transferflooz.util;

public class CommonValidations {

	public boolean validateStringValues(String reqParamValue) {
		boolean validateResult = false;
		if (reqParamValue == null || reqParamValue.equals(" ") || reqParamValue.equals(""))
			validateResult = true;

		return validateResult;
	}

	public boolean validateAmountValues(Double amount) {
		boolean validateResult = false;
		if (amount == null || amount < 0)
			validateResult = true;

		return validateResult;
	}

	public boolean validateNumber(String number) {
		boolean validateResult = false;
		if (number == null || !number.matches("[0-9]+"))
			validateResult = true;
		return validateResult;

	}
}
