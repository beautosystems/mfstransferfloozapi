package com.mfs.client.transferflooz.util;

public enum TransferFloozCodes {

	ER201("ER201", "MFS System Error"), 
	ER202("ER202", "Null response"), 
	ER204("ER204", "Exception while save data"),
	ER205("ER205", "Exception while update"), 
	VALIDATION_ERROR("ER206", "Validation Error"),
	REQUIRED_MSISDN("REQUIRED_MSISDN", "Provide The Correct MSISDN In The Form Of Numbers And Try Again."),
	REQUIRED_REMITTANCE_AMOUNT("REQUIRED_REMITTANCE_AMOUNT", "Provide The Correct Remittance Amount, Amount Can Not Be Negative And Try Again."),
	REQUIRED_BRAND_ID("REQUIRED_BRAND_ID", "Provide The Correct Brand Id And Try Again."),
	REQUIRED_MFS_TRANSACTION_ID("REQUIRED_MFS_TRANSACTION_ID", "Provide The Correct MFS Transactionn Id And Try Again."),
	ER210("ER210", "Sales order details not found"),
	ER211("ER211", "Exception while getting account msisdn and account id"),
	ER212("ER212", "Exception while getting Transaction log detail"),
	BOTH_ID_CANNOT_BE_NULL("BOTH_ID_CANNOT_BE_NULL", "Provide Account Msisdn or Account Id details"),
	ER214("ER214", "Null Access Token"),
	ER216("ER216", "Duplicate Transaction Id"),
	ER215("ER215", "Authorization data not found"),
	AUTHENTICATION_FAILURE("AUTHENTICATION_FAILURE",
			"Authentication failed due to invalid authentication credentials."),
	NOT_AUTHORIZED("NOT_AUTHORIZED", "Authorization failed due to insufficient permissions."),
	RESOURCE_NOT_FOUND("RESOURCE_NOT_FOUND", "The specified resource does not exist."),
	METHOD_NOT_SUPPORTED("METHOD_NOT_SUPPORTED", "he server does not implement the requested HTTP method."),
	MEDIA_TYPE_NOT_ACCEPTABLE("MEDIA_TYPE_NOT_ACCEPTABLE",
			"The server does not implement the media type that would be acceptable to the client."),
	UNSUPPORTED_MEDIA_TYPE("UNSUPPORTED_MEDIA_TYPE", "The server does not support the request payload’s media type."),
	UNPROCCESSABLE_ENTITY("UNPROCCESSABLE_ENTITY",
			"The API cannot complete the requested action, or the request action is semantically incorrect or fails business validation."),
	INVALID_REQUEST("INVALID_REQUEST", "Request is not well-formed, syntactically incorrect, or violates schema."),
	INVALID_MSISDN_OR_ACCOUNT_ID("INVALID_MSISDN_OR_ACCOUNT_ID","Provide correct Account Msisdn and Account Id");
	
	private String code;
	private String message;

	private TransferFloozCodes(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
